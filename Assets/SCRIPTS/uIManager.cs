﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class uIManager : MonoBehaviour
{

    public static bool isMenu;
    public static bool isGame;
    public static bool isGameEnd;
    public static bool isLevels;

    public GameObject UiAnimHolder;
    Animator uiAnim;

    public GameObject[] levelButtons;

    public Text menuUiLevelsText;
    public Text gameUiLevelsText;

    public static bool doUiUpdate;

    public GameObject MenuPanel;
    public GameObject LevelsPanel;
    public GameObject GameEndPanel;
    public GameObject GameCanvas;

    // Start is called before the first frame update
    void Start()
    {
        isMenu = true;
        DeterminePanels();
        //uiAnim = UiAnimHolder.GetComponent<Animator>();
        LoadLevelButtons();
        UpdateTexts();
    }

    // Update is called once per frame
    void Update()
    {
        if (doUiUpdate)
        {
            UpdateTexts();
            doUiUpdate = false;
        }
    }

    public void DeterminePanels()
    {
        if (isMenu)
        {
            //uiAnim.SetTrigger("GoToMenu");
            
            MenuPanel   .SetActive(true);
            LevelsPanel .SetActive(false);
            GameEndPanel.SetActive(false);
        }
        else if (isGame)
        {
            //uiAnim.SetTrigger("GoToGame");

            MenuPanel.SetActive(false);
            LevelsPanel.SetActive(false);
            GameEndPanel.SetActive(false);
        }
        else if (isGameEnd)
        {
            //uiAnim.SetTrigger("GoToGameEnd");

            MenuPanel.SetActive(false);
            LevelsPanel.SetActive(false);
            GameEndPanel.SetActive(true);
        }
        if (isLevels)
        {
            //uiAnim.SetTrigger("GoToLevels");

            MenuPanel.SetActive(false);
            LevelsPanel.SetActive(true);
            GameEndPanel.SetActive(false);
        }
    }

    void LoadLevelButtons()
    {
        for (int i = 0; i < levelButtons.Length; i++)
        {
            levelButtons[i].GetComponent<levelButtonManager>().levelOwn = i + 1;
            levelButtons[i].GetComponent<levelButtonManager>().UnlockIt();
        }
    }

    public void UpdateTexts()
    {
        
        menuUiLevelsText.text = "" + gameManagement.level;
        gameUiLevelsText.text = "" + gameManagement.level;
    }
}
