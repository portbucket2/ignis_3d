﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class burnCube : MonoBehaviour
{
    public bool active;
    public bool doubleHit;
    public bool gotNext;
    public LayerMask burnLayer;
    public float burnValue;

    public GameObject endSphere;
    public bool sphereDone;
    public bool raycasted;

    public ParticleSystem sparkParticle;
    //public GameObject buildingMesh;
    //public GameObject buildingAnim;
    public Animator buildAnim;
    public GameObject[] buildingMeshes;
    //public GameObject[] buildingBases;
    public GameObject[] vangas;
    public float[] vangaAngles;
    

    public bool resetCube;
    

    //public int i ;
    //public int a;

    // Start is called before the first frame update
    void Start()
    {
        int b = Random.Range(0, buildingMeshes.Length);
        

        for (int i = 0; i < buildingMeshes.Length; i++)
        {
            if(i == b)
            {
                buildingMeshes[i].SetActive(true);
                //buildingBases[i].SetActive(true);
            }
            else
            {
                buildingMeshes[i].SetActive(false);
                //buildingBases[i].SetActive(false);
            }
            
        }

        gameManagement.cubesInScene += 1;
        float bldScale = Random.Range(0.8f, 1.2f);
        
        //buildingMesh.transform.localScale = new Vector3(1, bldScale, 1);
        //buildAnim = buildingMesh.GetComponentInParent<Animator>();
        //buildAnim = buildingMesh.GetComponent<Animator>();

        AppearVanga();
    }

    // Update is called once per frame
    void Update()
    {
        if (active)
        {
            //RaycastingAround();
            
            Invoke("RaycastingAround", 0.2f);
            Invoke("HideOwn", 0.21f);
            
            //HideOwn();
        }

        //ResetCube();


    }

    void RaycastingAround()
    {
        if (!raycasted)
        {
            RaycastHit hit;
            if (Physics.Raycast(transform.position, transform.forward, out hit, 1f, burnLayer))
            {
                Debug.DrawRay(transform.position, transform.forward * hit.distance, Color.yellow);
                if (hit.transform.gameObject.GetComponent<burnCube>().active != true)
                {
                    hit.transform.gameObject.GetComponent<burnCube>().active = true;
                    gotNext = true;
                }
                else if(hit.transform.gameObject.GetComponent<burnCube>().active == true)
                {
                    hit.transform.gameObject.GetComponent<burnCube>().doubleHit = true;
                }


            }
            if (Physics.Raycast(transform.position, -transform.forward, out hit, 1f, burnLayer))
            {
                Debug.DrawRay(transform.position, -transform.forward * hit.distance, Color.yellow);
                if (hit.transform.gameObject.GetComponent<burnCube>().active != true)
                {
                    hit.transform.gameObject.GetComponent<burnCube>().active = true;
                    gotNext = true;
                }
                else if (hit.transform.gameObject.GetComponent<burnCube>().active == true)
                {
                    hit.transform.gameObject.GetComponent<burnCube>().doubleHit = true;
                }

            }
            if (Physics.Raycast(transform.position, transform.right, out hit, 1f, burnLayer))
            {
                Debug.DrawRay(transform.position, transform.right * hit.distance, Color.yellow);
                if (hit.transform.gameObject.GetComponent<burnCube>().active != true)
                {
                    hit.transform.gameObject.GetComponent<burnCube>().active = true;
                    gotNext = true;
                }
                else if (hit.transform.gameObject.GetComponent<burnCube>().active == true)
                {
                    hit.transform.gameObject.GetComponent<burnCube>().doubleHit = true;
                }

            }
            if (Physics.Raycast(transform.position, -transform.right, out hit, 1f, burnLayer))
            {
                Debug.DrawRay(transform.position, -transform.right * hit.distance, Color.yellow);
                if (hit.transform.gameObject.GetComponent<burnCube>().active != true)
                {
                    hit.transform.gameObject.GetComponent<burnCube>().active = true;
                    gotNext = true;
                }
                else if (hit.transform.gameObject.GetComponent<burnCube>().active == true)
                {
                    hit.transform.gameObject.GetComponent<burnCube>().doubleHit = true;
                }

            }
            sparkParticle.Play();
            
            //smallBlast.Play();
            buildAnim.SetTrigger("smash");
            this.GetComponent<BoxCollider>().enabled = false;
            raycasted = true;
            gameManagement.cubesInScene -= 1;
        }
        
    }

    void HideOwn()
    {
        //burnValue = Mathf.Clamp(burnValue, -1.1f, 1);
        
        this.GetComponent<Renderer>().material.SetFloat("Vector1_B4867CA0", burnValue);
        //burnValue += Time.deltaTime * 2f;
        //if (!gotNext && !sphereDone)
        //{
        //    Instantiate(endSphere, this.transform.position, Quaternion.identity);
        //    sphereDone = true;
        //}
        if (doubleHit && !sphereDone)
        {
            //Instantiate(endSphere, this.transform.position, Quaternion.identity);
            GameObject go = Instantiate(endSphere, this.transform.position, Quaternion.identity);
            go.transform.SetParent(transform);

            sphereDone = true;
        }
        
        
        //this.gameObject.SetActive(false);
    }

    void AppearVanga()
    {
        int i = Random.Range(0, 2);
        int a = Random.Range(0, 4);

        vangas[i].transform.localRotation = Quaternion.Euler( new Vector3(0, vangaAngles[a], 0));
        vangas[i].SetActive(true);
        Debug.Log("Vangsi");
    }

    //public void ResetCube()
    //{
    //    if (resetCube)
    //    {
    //        buildAnim.SetTrigger("repair");
    //        resetCube = false;
    //        active = false;
    //        gotNext = false;
    //        raycasted = false;
    //        sphereDone = false;
    //    }
    //}

    //void RaycastSelect()
    //{
    //    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
    //    RaycastHit hit;
    //    if (Physics.Raycast(ray, out hit, 100f, burnLayer))
    //    {
    //
    //    }
    //}
}
