﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class roadBlockMaker : MonoBehaviour
{
    public bool three_turns;
    public bool four_turns;

    

    public Material[] roadmats;
    // Start is called before the first frame update
    void Start()
    {
        DetermineTurnMat();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void DetermineTurnMat()
    {
        if (three_turns)
        {
            this.GetComponent<Renderer>().material = roadmats[1];
        }
        else if (four_turns)
        {
            this.GetComponent<Renderer>().material = roadmats[2];
        }
        else
        {
            this.GetComponent<Renderer>().material = roadmats[0];
        }
    }
}
