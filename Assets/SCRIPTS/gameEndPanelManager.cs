﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gameEndPanelManager : MonoBehaviour
{
    public static bool levelFailed;

    public GameObject faileurePanel;
    public GameObject successPanel;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (levelFailed)
        {
            faileurePanel.SetActive(true) ;
            successPanel.SetActive(false);
        }
        else
        {
            faileurePanel.SetActive(false);
            successPanel.SetActive(true);
        }
    }
}
