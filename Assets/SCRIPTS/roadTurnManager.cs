﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class roadTurnManager : MonoBehaviour
{
    public bool three_turns;
    public bool four_turns;

    public bool turnOff;

    //public float turnAngle;
    public GameObject[] cornerMeshes;

    public Material[] roadmats;
    public GameObject longRoad;
    public GameObject longRoadMesh;
    public GameObject nextBlock;
    public float lengthOfRoad;
    // Start is called before the first frame update
    void Start()
    {
        DetermineTurnMat();
    }

    // Update is called once per frame
    //void Update()
    //{
    //
    //}

    void DetermineTurnMat()
    {
        int meshId = 0;
        if (three_turns)
        {
            meshId = 1;
        }
        else if (four_turns)
        {
            meshId = 2;
        }
        else
        {
            meshId = 0;
        }

        for (int i = 0; i < cornerMeshes.Length; i++)
        {
            if(i == meshId && !turnOff)
            {
                cornerMeshes[i].SetActive(true);
                //cornerMeshes[i].transform.localRotation = Quaternion.Euler(new Vector3(90, turnAngle, 0));

            }
            else
            {
                cornerMeshes[i].SetActive(false);
            }
        }

        lengthOfRoad = Vector3.Distance(this.transform.position, nextBlock.transform.position);
        longRoad.transform.localScale = new Vector3(lengthOfRoad -1 , 1, 1);
        longRoadMesh.GetComponent<Renderer>().material.mainTextureScale= new Vector2( (lengthOfRoad - 1),1);
        



    }
}
