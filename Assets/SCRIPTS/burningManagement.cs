﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class burningManagement : MonoBehaviour
{
    //public GameObject burnblocksParent;
    public GameObject[] burnBlocks;
    public GameObject burnBallOne;
    public GameObject burnBallTwo;

    public GameObject endOfBurn;

    public int maxBlocks;

    public GameObject tappedBlock;
    public int tappedBlockId;
    public int endBlockId;

    public LayerMask burnLayer;

    public bool burning;
    public bool inRange;

    //public GameObject inRangeBlock;
    // Start is called before the first frame update
    void Start()
    {
        maxBlocks = burnBlocks.Length;
        endOfBurn.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastSelect();
        }
    }


    void RaycastSelect()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if(Physics.Raycast(ray, out hit, 100f, burnLayer))
        {
            Debug.Log("rrrr");
            if (!burning)
            {
                tappedBlock = hit.transform.gameObject;
                //inRange = true;
                hit.transform.gameObject.GetComponentInParent<burningManagement>().burning = true;
                //burning = true;

                
            }

            if (burning)
            {
                burnBallOne.GetComponent<burnballController>().parentSet = hit.transform.parent.gameObject.transform.parent.gameObject;
                burnBallTwo.GetComponent<burnballController>().parentSet = hit.transform.parent.gameObject.transform.parent.gameObject;




                for (int i = 0; i < burnBlocks.Length; i++)
                {
                    if (burnBlocks[i] == hit.transform.gameObject)
                    {
                        tappedBlockId = i;
                    }
                }
                burnBallOne.GetComponent<burnballController>().targetID = tappedBlockId;
                burnBallTwo.GetComponent<burnballController>().targetID = tappedBlockId;

                if (tappedBlockId > maxBlocks / 2)
                {
                    endBlockId = tappedBlockId - maxBlocks / 2;
                }
                else if (tappedBlockId <= maxBlocks / 2)
                {
                    endBlockId = tappedBlockId + maxBlocks / 2;
                }

                burnBallOne.SetActive(true);
                burnBallOne.transform.position = burnBlocks[tappedBlockId].transform.position;

                burnBallTwo.SetActive(true);
                burnBallTwo.transform.position = burnBlocks[tappedBlockId].transform.position;
            }

            endOfBurn.transform.position = burnBlocks[endBlockId].transform.position;
            endOfBurn.SetActive(true);
            
        }

        //if (inRange)
        //{
        //    inRangeBlock.GetComponentInParent<burningManagement>().burning = true;
        //    if (burning)
        //    {
        //        burnBallOne.GetComponent<burnballController>().parentSet = inRangeBlock.transform.parent.gameObject.transform.parent.gameObject;
        //        burnBallTwo.GetComponent<burnballController>().parentSet = inRangeBlock.transform.parent.gameObject.transform.parent.gameObject;
        //
        //
        //
        //
        //        for (int i = 0; i < burnBlocks.Length; i++)
        //        {
        //            if (burnBlocks[i] == inRangeBlock.transform.gameObject)
        //            {
        //                tappedBlockId = i;
        //            }
        //        }
        //        burnBallOne.GetComponent<burnballController>().tappedID = tappedBlockId;
        //        burnBallTwo.GetComponent<burnballController>().tappedID = tappedBlockId;
        //
        //        if (tappedBlockId > maxBlocks / 2)
        //        {
        //            endBlockId = tappedBlockId - maxBlocks / 2;
        //        }
        //        else if (tappedBlockId <= maxBlocks / 2)
        //        {
        //            endBlockId = tappedBlockId + maxBlocks / 2;
        //        }
        //
        //        burnBallOne.SetActive(true);
        //        burnBallOne.transform.position = burnBlocks[tappedBlockId].transform.position;
        //
        //        burnBallTwo.SetActive(true);
        //        burnBallTwo.transform.position = burnBlocks[tappedBlockId].transform.position;
        //    }
        //
        //    endOfBurn.transform.position = burnBlocks[endBlockId].transform.position;
        //    endOfBurn.SetActive(true);
        //}
    }
}
