﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class groundBlockmanager : MonoBehaviour
{
    public GameObject[] groundBlocks;
    // Start is called before the first frame update
    void Start()
    {
        int m = Random.Range(0, groundBlocks.Length);
        for (int i = 0; i < groundBlocks.Length; i++)
        {
            if (i == m)
            {
                groundBlocks[i].SetActive(true);
            }
            else
            {
                groundBlocks[i].SetActive(false);
            }
        }

        int a = Random.Range(0, 4);
        if (a == 0)
        {
            groundBlocks[m].transform.localRotation = Quaternion.Euler(new Vector3(0, 0, 0));
        }
        else if (a == 1)
        {
            groundBlocks[m].transform.localRotation = Quaternion.Euler(new Vector3(0, 90, 0));
        }
        else if (a == 2)
        {
            groundBlocks[m].transform.localRotation = Quaternion.Euler(new Vector3(0, 180, 0));
        }
        else if (a == 3)
        {
            groundBlocks[m].transform.localRotation = Quaternion.Euler(new Vector3(0, -90, 0));
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
