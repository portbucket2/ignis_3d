﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class personAnimManager : MonoBehaviour
{
    public Animator personAnim;
    public float speed;
    public GameObject[] personMesh;
    public GameObject indicator;
    // Start is called before the first frame update
    void Start()
    {
        speed = Random.Range(0.5f, 1f);
        personAnim.SetFloat("walkSpeed", speed);
        int m = Random.Range(0, personMesh.Length);
        for (int i = 0; i < personMesh.Length; i++)
        {
            if(i == m)
            {
                personMesh[i].SetActive(true);
            }
            else
            {
                personMesh[i].SetActive(false);
            }
        }
        indicator.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (gameManagement.runPerson)
        {
            personAnim.SetTrigger("run");
        }
    }
}
